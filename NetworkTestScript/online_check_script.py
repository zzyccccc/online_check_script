import sys
import asyncio
import openpyxl
from aioconsole import ainput, get_standard_streams
from ping3 import ping
import time
import os
import logging.config
from collections import defaultdict
import statistics
import socket

# 定义日志配置
log_config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "%(asctime)s - %(levelname)s - %(message)s"
        }
    },
    "handlers": {
        "file_handler": {
            "class": "logging.FileHandler",
            "filename": "connect_check_logger.log",
            "formatter": "simple",
            "encoding": "utf-8"
        }
    },
    "root": {
        "level": "INFO",
        "handlers": ["file_handler"]
    }
}

# 文件路径
file_path = ""
# 文件名
file_name = ""
# 连接失败的IP地址
connect_failed = set()
# 字典用于存储每个IP的连接状态和RTT（Round Trip Time）
connection_status = defaultdict(list)

# 程序执行周期(分钟)
execute_period = 0

# 定义一个标志，用于判断是否应该退出程序
should_exit = False


# 用socket对象检测端口是否正常
def check_port(ip, port):
    try:
        # 创建一个套接字对象
        sock = socket.create_connection((ip, port), timeout=4)
        sock.close()
        return True, None
    except (socket.error, socket.timeout) as e:
        return False, e


def calculate_online_status():
    logger.info("正在计算连接状态的统计信息...")
    for ip, rtt_list in connection_status.items():
        if rtt_list:
            avg_rtt = statistics.mean(rtt_list)
            max_rtt = max(rtt_list)
            min_rtt = min(rtt_list)
            logger.info(
                f"IP地址: {ip}, 平均延迟: {avg_rtt} ms, 最大延迟: {max_rtt} ms, 最小延迟: {min_rtt} ms")


def get_windows_desktop_path():
    desktop_path = os.path.join(os.path.expanduser('~'), 'Desktop')
    return desktop_path


# 桌面路径
desktop_path = get_windows_desktop_path()
# 当前文件路径
current_directory = os.getcwd()

# # 从配置文件中加载日志配置
# with open('F:\\Python_Project\\NetworkTestScript\\logging_config.json', 'rt') as f:
#     config = json.load(f)

# 配置日志记录器
logging.config.dictConfig(log_config)

# 创建日志记录器
logger = logging.getLogger('connect_check_logger')

# 创建一个控制台处理程序并添加到日志记录器
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)  # 设置控制台处理程序的日志级别
console_handler.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
logger.addHandler(console_handler)


async def main():
    global file_path
    global file_name
    global execute_period
    if not file_name:
        print("日志文件 connect_check_logger.log 已在同级目录下生成...")
        file_name = await ainput("请将文件拖入: ")
        execute_period = int(await ainput("请输入要执行的周期(分钟): "))

    if "\\" in file_name:
        file_path = file_name
    else:
        file_path = os.path.join(current_directory, file_name)
    # logger.info(f"file_path:{file_path}")


def online_check():
    if __name__ == "__main__":
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        try:
            loop.run_until_complete(main())
            loop.run_until_complete(get_standard_streams())
            workbook = openpyxl.load_workbook(file_path)
            sheet = workbook.active
            ip_column = -1
            port_column = -1
            deceive_column = -1
            start_row = 2
            for cell in sheet[1]:
                cell_value = str(cell.value)
                if "IP" in cell_value:
                    ip_column = cell.column
                if "端口" in cell_value:
                    port_column = cell.column
                if "设备名称" in cell_value:
                    deceive_column = cell.column
            if ip_column == -1:
                print("未配置IP信息...")
                sys.exit()
            logger.info("开始测试网络连通情况...")
            for row in sheet.iter_rows(min_row=start_row, values_only=True):
                ip_address = row[ip_column - 1]
                port = row[port_column - 1]
                deceive_name = row[deceive_column - 1]
                if ip_address:
                    response = ping(ip_address)
                    if response is not None:
                        # 更新连接状态和RTT
                        connection_status[ip_address].append(response)
                        try:
                            # 移除后续连通成功的IP地址
                            connect_failed.remove((ip_address, port))
                            logger.info(f"IP地址: {ip_address},端口:{port} 重新连通成功...")
                        except KeyError:
                            logger.info(f"设备名称:{deceive_name},IP地址: {ip_address},端口:{port} 正在进行连接...")
                            logger.info(f"Round Trip Time: {response} ms")
                            is_port_normal, error_message = check_port(ip_address, port)
                            if is_port_normal:
                                logger.info(f"连接到 {ip_address}:{port} 成功...")
                            else:
                                logger.info(
                                    f"连接到 {ip_address}:{port} 成功, 访问端口失败: {error_message} timeout 4 seconds...")
                    else:
                        logger.info(
                            f"设备名称:{deceive_name},IP地址: {ip_address},端口:{port} 网络连通失败,timeout 4 seconds...")
                        connect_failed.add((ip_address, port))
            logger.info(f"统计IP连通失败信息: {connect_failed}")
            # 开始计算连接状态的统计信息
            calculate_online_status()
            logger.info("网络状态检测完成...")
            logger.info("开始对失败的IP进行重新测试...")
            workbook.close()
        except FileNotFoundError:
            print(f"文件{file_path}未找到")
        except Exception as e:
            print(f"发生错误: {e}")


# 跟踪IP地址的失败次数
failed_count = {}


# 复查之前未连通的IP地址
def online_recheck():
    # 创建一个要删除的 IP 列表
    ips_to_remove = []

    # 进一步分析网络稳定性，例如，生成频繁失败的警报
    if len(connect_failed) > 0:
        for ip, port in connect_failed:
            response = ping(ip)
            if response is not None:
                is_port_normal, error_message = check_port(ip, port)
                if is_port_normal:
                    logger.info(f"IP地址: {ip}:{port} 重新连通成功...")
                else:
                    logger.info(f"连接到 {ip}:{port} 成功, 访问端口失败: {error_message} timeout 4 seconds...")
                    failed_count[(ip, port)] = failed_count.get((ip, port), 0) + 1
                ips_to_remove.append((ip, port))
            else:
                logger.info(f"IP地址: {ip}:{port} 重新连通失败, timeout 4 seconds...")
                failed_count[(ip, port)] = failed_count.get((ip, port), 0) + 1
                if failed_count[(ip, port)] >= 3:
                    logger.warning(
                        f"IP地址: {ip}:{port} 连续失败超过{failed_count.get((ip, port))}次，请检查网络稳定性...")
                    ips_to_remove.append((ip, port))
                else:
                    # 如果失败次数不超过三次，继续下一个IP的检查
                    continue

        # 批量删除失败的 IP 地址
        for ip, port in ips_to_remove:
            connect_failed.remove((ip, port))
            failed_count.pop((ip, port), None)
    else:
        logger.info("复查完成，正在退出程序...")
        # 设置标志，告知主循环等待 execute_period 后再次执行
        global should_exit
        should_exit = True


try:
    while True:
        online_check()
        start_time = time.time()  # 记录开始时间

        while not should_exit:
            online_recheck()
            time.sleep(1)  # 每秒执行一次 online_recheck
            # 如果应该退出，设置 should_exit 为 True
            if should_exit:
                break

        should_exit = False  # 重置退出标志

        # 计算执行时间
        end_time = time.time()
        execution_time = end_time - start_time
        wait_time = float(execute_period * 60)

        # 等待剩余的时间，确保执行周期为 execute_period
        logger.info(f"执行任务花费了{execution_time / 60:.2f}分钟,等待{execute_period}分钟后开始重新运行...")
        time.sleep(wait_time)

except (KeyboardInterrupt, SystemExit):
    # 在用户中断程序或系统退出时记录终止日志
    logger.info("程序被中断或终止...")
